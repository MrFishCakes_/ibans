package me.mrfishcakes.ibans.discord;

import com.tjplaysnow.discord.object.Bot;
import com.tjplaysnow.discord.object.CommandSpigotManager;
import com.tjplaysnow.discord.object.ThreadSpigot;
import lombok.Getter;
import me.mrfishcakes.ibans.IBans;

/**
 * iBans:
 * <p>
 * Created by MrFishCakes
 * Date: 20/05/2018.
 * Time: 19:02.
 */
public class DiscordBot {

    /* Private Variables */
    @Getter
    private String botToken;
    @Getter
    private String botPrefix;

    public DiscordBot(String botToken, String botPrefix) {
        this.botToken = botToken;
        this.botPrefix = botPrefix;
    }

    public void login() {
        IBans.getInstance().setDiscordBot(new Bot(botToken, botPrefix));
        IBans.getInstance().getDiscordBot().setBotThread(new ThreadSpigot(IBans.getInstance()));
        IBans.getInstance().getDiscordBot().setConsoleCommandManager(new CommandSpigotManager());
    }
}
