package me.mrfishcakes.ibans.logger;

import org.bukkit.Bukkit;

/**
 * iBans:
 * <p>
 * Created by MrFishCakes
 * Date: 18/05/2018.
 * Time: 20:11.
 */
public class Logger {

    /**
     * Log information to the server
     *
     * @param message The information message to print
     */
    public static void info(String message) {
        Bukkit.getLogger().info("[iBans] " + message);
    }

    /**
     * Log warnings to the server
     *
     * @param message The warning message to print
     */
    public static void warning(String message) {
        Bukkit.getLogger().warning("[iBans] " + message);
    }

    /**
     * Log severe warnings to the server
     *
     * @param message The severe warning message to print
     */
    public static void severe(String message) {
        Bukkit.getLogger().severe("[iBans] " + message);
    }

}
