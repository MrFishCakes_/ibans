package me.mrfishcakes.ibans.utils;

import me.mrfishcakes.ibans.logger.Logger;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * iBans:
 * <p>
 * Created by MrFishCakes
 * Date: 20/05/2018.
 * Time: 11:38.
 */
public class Updater {

    /* Private Variables */
    private int project = 0;
    private URL checkURL;
    private String newVersion = "";
    private JavaPlugin plugin;

    public Updater(JavaPlugin plugin, int projectId) {
        this.plugin = plugin;
        this.newVersion = plugin.getDescription().getVersion();
        this.project = projectId;
        try {
            this.checkURL = new URL("https://api.spigotmc.org/legacy/update.php?resource=" + projectId);
        } catch (MalformedURLException e) {
            Logger.severe("There was an error checking for updates:");
            e.printStackTrace();
        }
    }

    /**
     * Get the project ID
     *
     * @return The project ID
     */
    public int getProjectId() {
        return project;
    }

    /**
     * Get the plugin's main class
     *
     * @return The plugin being updated
     */
    public JavaPlugin getPlugin() {
        return plugin;
    }

    /**
     * Get the latest plugin version
     *
     * @return The latest version
     */
    public String getLatestVersion() {
        return newVersion;
    }

    /**
     * Get the resource URL
     *
     * @return The resource URL
     */
    public String getResourceUrl() {
        return "https://www.spigotmc.org/resources/" + project;
    }

    /**
     * Checks the plugin for updates
     *
     * @return Is the plugin outdated?
     */
    public boolean checkUpdates() {
        try {
            URLConnection connection = checkURL.openConnection();
            this.newVersion = new BufferedReader(new InputStreamReader(connection.getInputStream())).readLine();
            return !plugin.getDescription().getVersion().equals(newVersion);
        } catch (IOException e) {
            Logger.severe("There was en error checking for updates:");
            e.printStackTrace();
            return false;
        }
    }

}
