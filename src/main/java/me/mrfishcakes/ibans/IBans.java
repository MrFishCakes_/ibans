package me.mrfishcakes.ibans;

import com.tjplaysnow.discord.object.Bot;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.Getter;
import lombok.Setter;
import me.mrfishcakes.ibans.database.DatabaseManager;
import me.mrfishcakes.ibans.database.DatabaseType;
import me.mrfishcakes.ibans.discord.DiscordBot;
import me.mrfishcakes.ibans.events.Events;
import me.mrfishcakes.ibans.logger.Logger;
import me.mrfishcakes.ibans.utils.Updater;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * iBans:
 * <p>
 * Created by MrFishCakes
 * Date: 18/05/2018.
 * Time: 19:43.
 */
public final class IBans extends JavaPlugin {

    /* Private Variables */
    @Getter
    private static IBans instance;
    @Getter
    private HikariDataSource hikari;
    @Getter
    @Setter
    private String localFileName;
    @Getter
    private boolean checking = false;
    @Getter
    private boolean update = false;
    @Getter
    @Setter
    private Bot discordBot = null;
    @Getter
    private boolean discord = false;

    @Override
    public void onEnable() {
        final long start = System.currentTimeMillis();
        instance = this;
        Bukkit.getPluginManager().registerEvents(new Events(), this);
        saveDefaultConfig();

        checkUpdates();

        setupDatabase();

        setupDiscord();

        Logger.info("Plugin started in (" + ((System.currentTimeMillis() - start) / 1000L) + ")s");
    }

    @Override
    public void onDisable() {
        if (discordBot != null) {
            discordBot.logout();
            discordBot = null;
        }
        if (hikari != null || !hikari.isClosed()) {
            hikari.close();
            hikari = null;
        }
        localFileName = null;
        instance = null;
    }

    /**
     * Check the plugin for available updates
     */
    private void checkUpdates() {
        checking = getConfig().getBoolean("Plugin.CheckForUpdates");

        if (checking) {
            Logger.info("Checking for updates...");
            Updater updater = new Updater(this, 28081); //TODO Change ID to actual plugin when released.

            if (updater.checkUpdates()) {
                this.update = true;
                Logger.info("There is a new iBans version out! (v" + updater.getLatestVersion() + ")");
                Logger.info("It can be found at: " + updater.getResourceUrl());
            } else {
                Logger.info("No updates for iBans found!");
                Logger.info("You are running the latest version.");
            }
        }
    }

    /**
     * Setup the database for the plugin to use
     */
    private void setupDatabase() {
        Bukkit.getScheduler().runTaskAsynchronously(this, () -> {
            DatabaseType type = DatabaseType.fromString(getConfig().getString("Storage.Type"));
            DatabaseManager manager = new DatabaseManager(type);

            HikariConfig hikariConfig = new HikariConfig();

            hikariConfig.setDataSourceClassName(type.getDriver());

            if (manager.getDatabaseType() == DatabaseType.SQLITE) {
                localFileName = getConfig().getString("Storage.LocalFileName");
                try {
                    File databaseFile = new File(getDataFolder(), localFileName + ".db");

                    if (!databaseFile.exists()) {
                        Logger.info("File not found - Creating!");
                        databaseFile.createNewFile();
                        Logger.info("File was created in '/iBans/" + localFileName + ".db'");
                    }

                    hikariConfig.setJdbcUrl("jdbc:sqlite:" + getDataFolder() + File.separator + localFileName + ".db");

                    hikari = new HikariDataSource(hikariConfig);
                    try {
                        Connection connection = hikari.getConnection();
                        PreparedStatement preparedStatement = connection.prepareStatement("CREATE TABLE IF NOT EXISTS bans(id VARCHAR(36))");
                        preparedStatement.execute();

                        connection.close();
                        preparedStatement.close();

                        Logger.info("Connected to local database successfully!");
                        return;
                    } catch (SQLException e) {
                        Logger.severe("There was an error connecting to local database:");
                        e.printStackTrace();
                        return;
                    }
                } catch (IOException e) {
                    Logger.severe("There was an error connecting to local database:");
                    e.printStackTrace();
                    return;
                }
            } else if (manager.getDatabaseType() == DatabaseType.MYSQL || manager.getDatabaseType() == DatabaseType.POSTGRE) {
                hikariConfig.addDataSourceProperty("serverName", getConfig().getString("Storage.HostAddress"));
                hikariConfig.addDataSourceProperty("port", getConfig().getString("Storage.Port"));
                hikariConfig.addDataSourceProperty("databaseName", getConfig().getString("Storage.Database"));
                hikariConfig.addDataSourceProperty("user", getConfig().getString("Storage.Username"));
                hikariConfig.addDataSourceProperty("password", getConfig().getString("Storage.Password"));

                hikari = new HikariDataSource(hikariConfig);
                try {
                    Connection connection = hikari.getConnection();
                    PreparedStatement preparedStatement = connection.prepareStatement("CREATE TABLE IF NOT EXISTS bans(id VARCHAR(36))");
                    preparedStatement.execute();

                    connection.close();
                    preparedStatement.close();
                    Logger.info("Connected to database successfully @ " + getConfig().getString("Storage.HostAddress"));
                    return;
                } catch (SQLException e) {
                    Logger.severe("There was an error connecting to database @ " + getConfig().getString("Storage.HostAddress") + ":");
                    e.printStackTrace();
                    return;
                }
            } else {

                Logger.severe("Invalid config file, please delete it or check for errors");

                if (discordBot != null) {
                    discordBot.logout();
                    discordBot = null;
                }
                if (hikari != null || !hikari.isClosed()) {
                    hikari.close();
                    hikari = null;
                }
                localFileName = null;
                instance = null;

                Bukkit.getPluginManager().disablePlugin(this);
                return;
            }
        });
    }

    /**
     * Setup the DiscordBot if it is to be used
     */
    private void setupDiscord() {
        this.discord = getConfig().getBoolean("Plugin.DiscordBot.Enabled");

        if (discord) {
            if (!Bukkit.getPluginManager().isPluginEnabled("Discord-ProgramBot-API")) {
                Logger.severe("Tried to use Discord Bot with no API present!");
                Logger.severe("Download the API from: https://www.spigotmc.org/resources/49783");

                if (discordBot != null) {
                    discordBot.logout();
                    discordBot = null;
                }
                if (hikari != null || !hikari.isClosed()) {
                    hikari.close();
                    hikari = null;
                }
                localFileName = null;
                instance = null;

                Bukkit.getPluginManager().disablePlugin(this);
            } else {
                new DiscordBot(getConfig().getString("Plugin.DiscordBot.Token"), getConfig()
                        .getString("Plugin.DiscordBot.Prefix")).login();

                discordBot.addAction(() ->
                                discordBot.setGame(getConfig().getString("Plugin.DiscordBot.Game"))
                        , 1);
            }
        }
    }

}
