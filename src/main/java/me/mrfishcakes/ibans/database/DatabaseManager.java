package me.mrfishcakes.ibans.database;

import lombok.Getter;

/**
 * iBans:
 * <p>
 * Created by MrFishCakes
 * Date: 18/05/2018.
 * Time: 19:55.
 */
public class DatabaseManager {

    /* Private Variables */
    @Getter
    private DatabaseType databaseType = DatabaseType.SQLITE;

    /**
     * Class constructor to manage the database connections
     *
     * @param databaseType The database type that has been set in the config
     */
    public DatabaseManager(DatabaseType databaseType) {
        if (databaseType != DatabaseType.SQLITE) {
            this.databaseType = databaseType;
        }
    }

}
