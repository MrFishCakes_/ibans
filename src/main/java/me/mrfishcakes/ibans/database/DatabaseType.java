package me.mrfishcakes.ibans.database;

import lombok.Getter;

/**
 * iBans:
 * <p>
 * Created by MrFishCakes
 * Date: 18/05/2018.
 * Time: 19:55.
 */
public enum DatabaseType {


    SQLITE("org.sqlite.SQLiteDataSource"), MYSQL("com.mysql.jdbc.jdbc2.optional.MysqlDataSource"),

    POSTGRE("org.postgresql.ds.PGSimpleDataSource");


    /* Private Variables */
    @Getter
    private String driver;

    DatabaseType(String driver) {
        this.driver = driver;
    }

    /**
     * Get a DatabaseType from a string
     *
     * @param type The database type wish to be returned
     * @return The Database Type
     * @see DatabaseType
     */
    public static DatabaseType fromString(String type) {
        if (type.equalsIgnoreCase("sqlite") || type.equalsIgnoreCase("local")) {
            return SQLITE;
        } else if (type.equalsIgnoreCase("mysql")) {
            return MYSQL;
        } else if (type.equalsIgnoreCase("postgre")) {
            return POSTGRE;
        }
        return SQLITE;
    }

}
