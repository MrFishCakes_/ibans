package me.mrfishcakes.ibans.punishment;

/**
 * iBans:
 * <p>
 * Created by MrFishCakes
 * Date: 20/05/2018.
 * Time: 11:32.
 */
public enum PunishmentType {

    KICK, WARN, MUTE, BAN;
}
