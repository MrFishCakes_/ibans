package me.mrfishcakes.ibans.events;

import me.mrfishcakes.ibans.IBans;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * iBans:
 * <p>
 * Created by MrFishCakes
 * Date: 20/05/2018.
 * Time: 11:55.
 */
public class Events implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        final Player player = event.getPlayer();
        if (player.hasPermission("ibans.update")) {
            if (IBans.getInstance().isChecking()) {
                if (IBans.getInstance().isUpdate()) {
                    player.sendMessage(ChatColor.RED + "There is a new version of iBans out, download it now!");
                } else {
                    player.sendMessage(ChatColor.GREEN + "You are running the latest iBans version!");
                }
            }
        }
    }

}
