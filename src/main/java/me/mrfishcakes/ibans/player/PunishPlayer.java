package me.mrfishcakes.ibans.player;

import lombok.Getter;
import lombok.Setter;
import me.mrfishcakes.ibans.logger.Logger;
import me.mrfishcakes.ibans.punishment.Ban;
import me.mrfishcakes.ibans.punishment.Mute;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * iBans:
 * <p>
 * Created by MrFishCakes
 * Date: 19/05/2018.
 * Time: 21:26.
 */
public class PunishPlayer {

    /* Private Variables */
    @Getter
    @Setter
    private UUID uniqueId;
    @Getter
    private List<Ban> bans;
    @Getter
    private List<Mute> mutes;

    /**
     * Class constructor for a PunishPlayer
     *
     * @param uniqueId The UUID of the Player
     */
    public PunishPlayer(UUID uniqueId) {
        this.uniqueId = uniqueId;
        this.bans = new ArrayList<>();
        this.mutes = new ArrayList<>();
    }

    /**
     * Add a ban to the list of bans
     *
     * @param ban The ban to add
     */
    public void addBan(Ban ban) {
        if (ban == null) return;

        this.bans.add(ban);
        Logger.info("New ban entry added for: " + uniqueId);
    }

    /**
     * Add a mute to the list of mutes
     *
     * @param mute The mute to add
     */
    public void addMute(Mute mute) {
        if (mute == null) return;

        this.mutes.add(mute);
        Logger.info("New mute entry added for: " + uniqueId);
    }

    public int getTotalBans() {
        if (bans.size() > 0) {
            return bans.size();
        }
        return 0;
    }

    public int getTotalMutes() {
        if (mutes.size() > 0) {
            return mutes.size();
        }
        return 0;
    }

}
